package com.yun27jin;

import com.yun27jin.config.Factory;
import com.yun27jin.dao.UserDao;
import com.yun27jin.model.Level;
import com.yun27jin.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class UserDaoTest {
    private UserDao userDao;

    @Before
    public void setUp() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Factory.class);
        this.userDao = applicationContext.getBean("userDao", UserDao.class);
        userDao.add(new User("yunjin", "Jang Yunjin", Level.BASIC, 0, 0));
        userDao.add(new User("yuna", "Jang Yunjin", Level.BASIC, 0, 0));
        userDao.add(new User("jisung", "Jang Yunjin", Level.BASIC, 0, 0));
    }

    @Test
    public void getAll() {
        List<User> users = userDao.get();
        for (User user : users) {
            System.out.println(user);
        }
    }

    @Test
    public void test() {

    }

    @After
    public void delete() {
        userDao.delete();
    }

}