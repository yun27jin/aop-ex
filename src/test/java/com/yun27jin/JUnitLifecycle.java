package com.yun27jin;

import org.junit.*;

public class JUnitLifecycle {

    @BeforeClass
    public static void runsOnceBeforeClass() {
        System.out.println("@BeforeClass");
    }

    public JUnitLifecycle() {
        System.out.println("Constructor");
    }

    @Before
    public void a() {
        System.out.println("@Before a");
    }

    @Before
    public void b() {
        System.out.println("@Before b");
    }

    @Before
    public void cd() {
        System.out.println("@Before cd");
    }

    @Before
    public void ca() {
        System.out.println("@Before ca");
    }

    @Before
    public void cc() {
        System.out.println("@Before cc");
    }

    @Before
    public void d() {
        System.out.println("@Before d");
    }

    @Before
    public void e() {
        System.out.println("@Before e");
    }

    @Test
    public void firstTest() {
        System.out.println("@Test 1");
    }

    @Test
    public void secondTest() {
        System.out.println("@Test 2");
    }

    @After
    public void runsAfterEveryTestMethod() {
        System.out.println("@After");
    }

    @AfterClass
    public static void runsOnceAfterClass() {
        System.out.println("@AfterClass");
    }
}
