package com.yun27jin;

import com.yun27jin.config.Factory;
import com.yun27jin.dao.UserDao;
import com.yun27jin.model.Level;
import com.yun27jin.model.User;
import com.yun27jin.service.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Factory.class})
public class UserServiceTest {
    @Autowired
    private UserService userService;
    @Autowired
    private UserDao userDao;

    @Before
    public void setUp() {
        assertNotNull(this.userService);
        List<User> users = Arrays.asList(
                new User("id1", "name1", Level.BASIC, 0, 0),
                new User("id2", "name2", Level.BASIC, 49, 0),
                new User("id3", "name3", Level.BASIC, 50, 0),
                new User("id4", "name4", Level.BASIC, 51, 0),
                new User("id5", "name5", Level.SILVER, 60, 29),
                new User("id6", "name6", Level.SILVER, 60, 30),
                new User("id7", "name7", Level.GOLD, 60, 31)
        );
        userDao.add(users);
        for (User user : userDao.get()) {
            System.out.println(user);
        }
    }

    @Test
    public void test() {
        List<User> users = userDao.get();
        for (User user : users) {
            Boolean changed = null;
            Level level = user.getLevel();
            if (level == Level.BASIC && user.getLogin() >= 50) {
                user.setLevel(Level.SILVER);
                changed = true;
            } else if (level == Level.SILVER && user.getRecommend() >= 30) {
                user.setLevel(Level.GOLD);
                changed = true;
            } else if (level == Level.GOLD) {
                changed = false;
            } else {
                changed = false;
            }
            if (changed == true) {
                userDao.update(user);
            }
        }
        for (User user : users) {
            System.out.println(user);
        }

    }

    @After
    public void deleteAll() {
        userDao.delete();
    }

}