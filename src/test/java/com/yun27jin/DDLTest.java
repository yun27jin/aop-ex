package com.yun27jin;

import com.yun27jin.config.Factory;
import com.yun27jin.dao.UserDao;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DDLTest {
    private UserDao userDao;

    @Before
    public void initializeDI(){
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Factory.class);
        this.userDao = applicationContext.getBean("userDao", UserDao.class);
    }

    @Test
    public void executeDDL() {
        userDao.create();
    }

}