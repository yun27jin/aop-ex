package com.yun27jin.dao;

import com.yun27jin.model.Level;
import com.yun27jin.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class UserDaoJdbc implements UserDao {
    private JdbcTemplate jdbcTemplate;

    public UserDaoJdbc setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        return this;
    }

    @Override
    public void add(User user) {
        jdbcTemplate.update("INSERT INTO users(id, name, level, login, recommend) VALUES (?, ?, ?, ?, ?)",
                new Object[]{user.getId(),
                        user.getName(),
                        user.getLevel() == null ? Level.BASIC.intValue() : user.getLevel().intValue(),
                        user.getLogin(),
                        user.getRecommend()}
        );
    }

    @Override
    public void add(List<User> users) {
        final List<Object[]> parametersList = new ArrayList<>();
        IntStream.range(0, users.size()).forEach(i -> parametersList.add(
                new Object[]{users.get(i).getId(),
                        users.get(i).getName(),
                        users.get(i).getLevel() == null ? Level.BASIC.intValue() : users.get(i).getLevel().intValue(),
                        users.get(i).getLogin(),
                        users.get(i).getRecommend()}
        ));
        jdbcTemplate.batchUpdate("INSERT INTO users(id, name, level, login, recommend) VALUES (?, ?, ?, ?, ?)", parametersList);
    }

    @Override
    public int update(User user) {
        return jdbcTemplate.update("UPDATE users " +
                        "SET name=?, level=?, login=?, recommend=? " +
                        "WHERE id = ?",
                new Object[]{user.getName(),
                        user.getLevel().intValue(),
                        user.getLogin(),
                        user.getRecommend(),
                        user.getId()});
    }

    @Override
    public int delete(User user) {
        return jdbcTemplate.update("DELETE FROM users WHERE id = ?", user.getId());
    }

    @Override
    public int delete() {
        return jdbcTemplate.update("DELETE FROM users");
    }

    @Override
    public User get(User user) {
        List<User> users = jdbcTemplate.query("SELECT * FROM users WHERE id = ?", new Object[]{user.getId()},
                userRowMapper());
        if (users.isEmpty()) {
            return null;
        }
        return users.get(0);
    }

    @Override
    public List<User> get() {
        return jdbcTemplate.query("SELECT * FROM users", userRowMapper());
    }

    @Override
    public void create() {
        jdbcTemplate.execute("CREATE TABLE users (" +
                "id varchar(15) PRIMARY KEY," +
                "name varchar(15) NOT NULL," +
                "level tinyint NOT NULL," +
                "login int NOT NULL," +
                "recommend int NOT NULL)");
    }

    @Override
    public void drop() {
        jdbcTemplate.execute("DROP TABLE users");
    }

    private RowMapper<User> userRowMapper() {
        return (rs, i) -> new User(rs.getString("ID"),
                rs.getString("NAME"),
                Level.valueOf(rs.getInt("LEVEL")),
                rs.getInt("LOGIN"),
                rs.getInt("RECOMMEND")
        );
    }

}
