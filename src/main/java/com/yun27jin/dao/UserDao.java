package com.yun27jin.dao;

import com.yun27jin.model.User;

import java.util.List;

public interface UserDao {

    void add(User user);
    void add(List<User> user);
    User get(User user);
    List<User> get();
    int delete(User user);
    int delete();
    int update(User user);
    void create();
    void drop();

}