package com.yun27jin.config;

import com.yun27jin.dao.UserDao;
import com.yun27jin.dao.UserDaoJdbc;
import com.yun27jin.service.UserService;
import org.h2.Driver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;

@Configuration
public class Factory {

    @Bean
    public UserDao userDao() {
        return new UserDaoJdbc().setJdbcTemplate(jdbcTemplate());
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource());
        return jdbcTemplate;
    }

    @Bean
    public DataSource dataSource() {
        DataSource dataSource = new SimpleDriverDataSource();
        ((SimpleDriverDataSource) dataSource).setDriver(new Driver());
        ((SimpleDriverDataSource) dataSource).setUrl("jdbc:h2:tcp://localhost/~/test");
        ((SimpleDriverDataSource) dataSource).setUsername("sa");
        ((SimpleDriverDataSource) dataSource).setPassword("");
        return dataSource;
    }

    @Bean
    public UserService userService() {
        return new UserService().setUserDao(userDao());
    }

}