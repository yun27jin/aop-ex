package com.yun27jin.service;

import com.yun27jin.dao.UserDao;
import com.yun27jin.model.Level;
import com.yun27jin.model.User;

public class UserService {
    private UserDao userDao;

    public UserService setUserDao(UserDao userDao) {
        this.userDao = userDao;
        return this;
    }

    public void updateLevel(User user) {

    }

    public boolean canUpgradeLevel(User user) {
        Level currentLevel = user.getLevel();
        switch (currentLevel) {
            case BASIC:
                return user.getLogin() >= 50;
            case SILVER:
                return user.getRecommend() >= 30;
            case GOLD:
                return false;
            default:
                throw new IllegalArgumentException("Unknown currentLevel: " + currentLevel);
        }
    }


}
